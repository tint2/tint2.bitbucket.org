#include <X11/Xlib.h>
#include <X11/extensions/Xrandr.h>
#include <stdio.h>

int main()
{
  Display* dpy = XOpenDisplay(NULL);
  XRRScreenResources *res = XRRGetScreenResources ( dpy, DefaultRootWindow(dpy) );


  int		o;
  for (o = 0; o < res->noutput; o++) {
    XRROutputInfo* output_info = XRRGetOutputInfo(dpy, res, res->outputs[o]);
    printf( "Name: %s\n", output_info->name );
  }
}
